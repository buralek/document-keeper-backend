db = db.getSiblingDB('salesCheckServiceDB');
db.createUser({
    user: "salesCheckUser",
    pwd: "salesCheckDBPassword",
    roles: [
        {
            role: "readWrite",
            db: "salesCheckServiceDB"
        }
    ]
});
db.createCollection("sales_check");

db.createCollection("type");
db.type.insert({'_id': '2bf73919-b664-4438-8540-23a027bfaf9d', 'name' : 'clothes'});
db.type.insert({'_id': 'bd9851ad-71ec-42bc-9993-6588cb4030e0', 'name' : 'equipment'});
db.type.insert({'_id': 'b0f8e5fe-f8c2-48ca-ba75-bf4a056fbc2b', 'name' : 'plumbing'});
db.type.insert({'_id': '1d0a74fb-1794-4729-843b-93b86b6ad16c', 'name' : 'furniture'});
db.type.insert({'_id': 'cee88155-ec68-4569-8a8d-8ed71256aafb', 'name' : 'car'});
db.type.insert({'_id': 'e54eea7d-297f-469c-95b7-201c352c8f53', 'name' : 'hobby'});
