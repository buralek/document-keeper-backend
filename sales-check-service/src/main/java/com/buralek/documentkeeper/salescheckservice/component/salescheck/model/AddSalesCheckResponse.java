package com.buralek.documentkeeper.salescheckservice.component.salescheck.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.StringJoiner;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AddSalesCheckResponse {
    private String addedSalesCheckId;

    public String getAddedSalesCheckId() {
        return addedSalesCheckId;
    }

    public void setAddedSalesCheckId(String addedSalesCheckId) {
        this.addedSalesCheckId = addedSalesCheckId;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", AddSalesCheckResponse.class.getSimpleName() + "[", "]")
                .add("addedSalesCheckId='" + addedSalesCheckId + "'")
                .toString();
    }
}
