package com.buralek.documentkeeper.salescheckservice.service.mongodb.builder;

import com.buralek.documentkeeper.salescheckservice.exception.SalesCheckServiceCreateDtoException;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.SalesCheckEntity;

import java.util.Date;
import java.util.List;

public class SalesCheckEntityBuilderImpl implements SalesCheckEntityBuilder {
    private String id;
    private String name;
    private String typeId;
    private String companyId;
    private String userName;
    private Date purchaseDate;
    private List<String> checkImages;
    private String warrantyId;

    @Override
    public SalesCheckEntityBuilder getBuilderInstance() {
        return this;
    }

    @Override
    public SalesCheckEntity createObject() {
        return new SalesCheckEntity();
    }

    @Override
    public SalesCheckEntityBuilder setId(String id) {
        this.id = id;
        return getBuilderInstance();
    }

    @Override
    public SalesCheckEntityBuilder setName(String name) {
        this.name = name;
        return getBuilderInstance();
    }

    @Override
    public SalesCheckEntityBuilder setTypeId(String typeId) {
        this.typeId = typeId;
        return getBuilderInstance();
    }

    @Override
    public SalesCheckEntityBuilder setCompanyId(String companyId) {
        this.companyId = companyId;
        return getBuilderInstance();
    }

    @Override
    public SalesCheckEntityBuilder setUserName(String userName) {
        this.userName = userName;
        return getBuilderInstance();
    }

    @Override
    public SalesCheckEntityBuilder setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
        return getBuilderInstance();
    }

    @Override
    public SalesCheckEntityBuilder setCheckImages(List<String> checkImages) {
        this.checkImages = checkImages;
        return getBuilderInstance();
    }

    @Override
    public SalesCheckEntityBuilder setWarrantyId(String warrantyId) {
        this.warrantyId = warrantyId;
        return getBuilderInstance();
    }

    @Override
    public SalesCheckEntity build() {
        final SalesCheckEntity salesCheckEntity = createObject();
        if (id == null || id.isEmpty()) {
            throw new SalesCheckServiceCreateDtoException("Can't create SalesCheckDto because Id is empty");
        }

        salesCheckEntity.setId(id);
        salesCheckEntity.setName(name);
        salesCheckEntity.setTypeId(typeId);
        salesCheckEntity.setPurchaseDate(purchaseDate);
        salesCheckEntity.setCheckImages(checkImages);
        salesCheckEntity.setCompanyId(companyId);
        salesCheckEntity.setUserName(userName);
        salesCheckEntity.setWarrantyId(warrantyId);
        return salesCheckEntity;
    }
}
