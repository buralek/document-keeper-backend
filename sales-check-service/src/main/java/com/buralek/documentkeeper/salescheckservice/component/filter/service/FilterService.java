package com.buralek.documentkeeper.salescheckservice.component.filter.service;

import com.buralek.documentkeeper.salescheckservice.component.filter.model.GetFilteredSalesChecksRequest;
import com.buralek.documentkeeper.salescheckservice.component.filter.model.GetFilteredSalesChecksResponse;

public interface FilterService {
    GetFilteredSalesChecksResponse filterSalesChecks(GetFilteredSalesChecksRequest request);
}
