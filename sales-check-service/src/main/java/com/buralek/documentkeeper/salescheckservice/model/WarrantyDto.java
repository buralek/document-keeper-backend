package com.buralek.documentkeeper.salescheckservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class WarrantyDto {
    private String id;
    private Date fromDate;
    private Date toDate;
    private List<String> warrantyImages;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public List<String> getWarrantyImages() {
        return warrantyImages;
    }

    public void setWarrantyImages(List<String> warrantyImages) {
        this.warrantyImages = warrantyImages;
    }

    @Override
    public String toString() {
        return "WarrantyDto{" +
                "id='" + id + '\'' +
                ", fromDate=" + fromDate +
                ", toDate=" + toDate +
                ", warrantyImages=" + warrantyImages +
                '}';
    }
}
