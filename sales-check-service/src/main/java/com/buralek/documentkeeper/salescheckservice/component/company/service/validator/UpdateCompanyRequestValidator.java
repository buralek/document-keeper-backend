package com.buralek.documentkeeper.salescheckservice.component.company.service.validator;

import com.buralek.documentkeeper.salescheckservice.component.company.model.UpdateCompanyDto;
import com.buralek.documentkeeper.salescheckservice.component.company.model.UpdateCompanyRequest;
import com.buralek.documentkeeper.salescheckservice.exception.SalesCheckServiceValidatorException;
import com.buralek.documentkeeper.salescheckservice.service.validator.Validator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component("updateCompanyRequestValidator")
public class UpdateCompanyRequestValidator implements Validator<UpdateCompanyRequest> {
    @Override
    public void validate(final UpdateCompanyRequest validatedObject) {
        final StringBuilder exceptionBuilder = new StringBuilder();
        final UpdateCompanyDto updatePart = validatedObject.getUpdatePart();
        if (StringUtils.isEmpty(validatedObject.getId())) {
            exceptionBuilder.append("\nCan't find a company because id is empty.");
        }
        if (updatePart == null) {
            exceptionBuilder.append("\nUpdate part is empty in the request.");
        }
        if (StringUtils.isEmpty(exceptionBuilder.toString())) {
            validateUpdatedPart(updatePart, exceptionBuilder);
        }

        if (StringUtils.isNotEmpty(exceptionBuilder.toString())) {
            throw new SalesCheckServiceValidatorException("Update Company request is wrong." + exceptionBuilder.toString());
        }
    }

    private void validateUpdatedPart(final UpdateCompanyDto updatePart, final StringBuilder exceptionBuilder) {
        if (updatePart.getName() != null && updatePart.getName().isEmpty()) {
            exceptionBuilder.append("\nUpdated name can't be empty.");
        }
    }

}
