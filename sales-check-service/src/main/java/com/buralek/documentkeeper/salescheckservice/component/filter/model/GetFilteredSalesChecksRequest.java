package com.buralek.documentkeeper.salescheckservice.component.filter.model;

import java.util.Date;
import java.util.StringJoiner;

public class GetFilteredSalesChecksRequest {
    private String id;
    private String name;
    private String typeId;
    private String companyName;
    private Date fromDate;
    private Date toDate;
    private String userName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", GetFilteredSalesChecksRequest.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("name='" + name + "'")
                .add("typeId='" + typeId + "'")
                .add("companyName='" + companyName + "'")
                .add("fromDate=" + fromDate)
                .add("toDate=" + toDate)
                .add("userName='" + userName + "'")
                .toString();
    }
}
