package com.buralek.documentkeeper.salescheckservice.component.salescheck.model;

import com.buralek.documentkeeper.salescheckservice.model.WarrantyDto;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UpdateSalesCheckDto {
    private String name;
    private String typeId;
    private Date purchaseDate;
    private String companyId;
    private WarrantyDto warranty;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public WarrantyDto getWarranty() {
        return warranty;
    }

    public void setWarranty(WarrantyDto warranty) {
        this.warranty = warranty;
    }

    @Override
    public String toString() {
        return "UpdateSalesCheckDto{" +
                "name='" + name + '\'' +
                ", typeId='" + typeId + '\'' +
                ", purchaseDate=" + purchaseDate +
                ", companyId='" + companyId + '\'' +
                ", warranty=" + warranty +
                '}';
    }
}
