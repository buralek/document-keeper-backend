package com.buralek.documentkeeper.salescheckservice.component.filter.service.validator;

import com.buralek.documentkeeper.salescheckservice.component.filter.model.GetFilteredSalesChecksRequest;
import com.buralek.documentkeeper.salescheckservice.exception.SalesCheckServiceValidatorException;
import com.buralek.documentkeeper.salescheckservice.service.validator.Validator;
import org.springframework.stereotype.Component;

@Component("FilterSalesChecksValidator")
public class FilterSalesChecksValidator implements Validator<GetFilteredSalesChecksRequest> {
    @Override
    public void validate(final GetFilteredSalesChecksRequest validatedObject) {
        // validate userName
        final String userName = validatedObject.getUserName();
        if (userName == null || userName.isEmpty()) {
            throw new SalesCheckServiceValidatorException("Wrong filter request. User id can't be empty.");
        }
    }
}
