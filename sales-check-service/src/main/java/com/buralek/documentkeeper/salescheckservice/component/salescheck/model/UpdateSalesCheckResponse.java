package com.buralek.documentkeeper.salescheckservice.component.salescheck.model;

import com.buralek.documentkeeper.salescheckservice.model.SalesCheckDto;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.StringJoiner;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UpdateSalesCheckResponse {
    private SalesCheckDto salesCheck;

    public SalesCheckDto getSalesCheck() {
        return salesCheck;
    }

    public void setSalesCheck(SalesCheckDto salesCheck) {
        this.salesCheck = salesCheck;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", UpdateSalesCheckResponse.class.getSimpleName() + "[", "]")
                .add("salesCheck=" + salesCheck)
                .toString();
    }
}
