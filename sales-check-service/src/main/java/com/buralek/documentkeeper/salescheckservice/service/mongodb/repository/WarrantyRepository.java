package com.buralek.documentkeeper.salescheckservice.service.mongodb.repository;

import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.WarrantyEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface WarrantyRepository extends MongoRepository<WarrantyEntity, String> {
}
