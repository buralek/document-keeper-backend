package com.buralek.documentkeeper.salescheckservice.model;

import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.CompanyEntity;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.TypeEntity;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.WarrantyEntity;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SalesCheckDto {
    private String id;
    private String name;
    private TypeDto type;
    private String userName;
    private CompanyDto company;
    private Date purchaseDate;
    private List<String> checkImages;
    private WarrantyDto warranty;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public List<String> getCheckImages() {
        return checkImages;
    }

    public void setCheckImages(List<String> checkImages) {
        this.checkImages = checkImages;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public TypeDto getType() {
        return type;
    }

    public void setType(TypeDto type) {
        this.type = type;
    }

    public CompanyDto getCompany() {
        return company;
    }

    public void setCompany(CompanyDto company) {
        this.company = company;
    }

    public WarrantyDto getWarranty() {
        return warranty;
    }

    public void setWarranty(WarrantyDto warranty) {
        this.warranty = warranty;
    }

    @Override
    public String toString() {
        return "SalesCheckDto{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", userName='" + userName + '\'' +
                ", company=" + company +
                ", purchaseDate=" + purchaseDate +
                ", checkImages=" + checkImages +
                ", warranty=" + warranty +
                '}';
    }
}
