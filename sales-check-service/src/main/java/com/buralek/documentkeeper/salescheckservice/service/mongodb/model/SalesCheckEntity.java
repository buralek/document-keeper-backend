package com.buralek.documentkeeper.salescheckservice.service.mongodb.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

@Document(collection = "sales_check")
public class SalesCheckEntity {
    @Id
    private String id;
    private String name;
    private String typeId;
    private String userName;
    private String companyId;
    private Date purchaseDate;
    private List<String> checkImages;
    private String warrantyId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public List<String> getCheckImages() {
        return checkImages;
    }

    public void setCheckImages(List<String> checkImages) {
        this.checkImages = checkImages;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getWarrantyId() {
        return warrantyId;
    }

    public void setWarrantyId(String warrantyId) {
        this.warrantyId = warrantyId;
    }

    @Override
    public String toString() {
        return "SalesCheckEntity{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", typeId='" + typeId + '\'' +
                ", userName='" + userName + '\'' +
                ", companyId='" + companyId + '\'' +
                ", purchaseDate=" + purchaseDate +
                ", checkImages=" + checkImages +
                ", warrantyId='" + warrantyId + '\'' +
                '}';
    }
}
