package com.buralek.documentkeeper.salescheckservice.component.salescheck.service.validator;

import com.buralek.documentkeeper.salescheckservice.component.salescheck.model.UpdateSalesCheckDto;
import com.buralek.documentkeeper.salescheckservice.component.salescheck.model.UpdateSalesCheckRequest;
import com.buralek.documentkeeper.salescheckservice.exception.SalesCheckServiceValidatorException;
import com.buralek.documentkeeper.salescheckservice.model.WarrantyDto;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.SalesCheckEntity;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.WarrantyEntity;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.repository.SalesCheckRepository;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.repository.WarrantyRepository;
import com.buralek.documentkeeper.salescheckservice.service.validator.Validator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service("updateSalesCheckRequestValidator")
public class UpdateSalesCheckRequestValidator implements Validator<UpdateSalesCheckRequest> {

    @Autowired
    private SalesCheckRepository salesCheckRepository;

    @Autowired
    private WarrantyRepository warrantyRepository;

    @Override
    public void validate(final UpdateSalesCheckRequest validatedObject) {
        final StringBuilder exceptionBuilder = new StringBuilder();
        final UpdateSalesCheckDto updatePart = validatedObject.getUpdatePart();
        if (StringUtils.isEmpty(validatedObject.getId())) {
            exceptionBuilder.append("\nCan't find a sales check because id is empty.");
        }
        if (StringUtils.isEmpty(validatedObject.getUserName())) {
            exceptionBuilder.append("\nCan't find a sales check because username is empty.");
        }
        if (updatePart == null) {
            exceptionBuilder.append("\nUpdate part is empty in the request.");
        }
        if (StringUtils.isEmpty(exceptionBuilder.toString())) {
            validateUpdatedPart(validatedObject, exceptionBuilder);
        }

        if (StringUtils.isNotEmpty(exceptionBuilder.toString())) {
            throw new SalesCheckServiceValidatorException("Update Sales Check request is wrong." + exceptionBuilder.toString());
        }
    }

    private void validateUpdatedPart(final UpdateSalesCheckRequest validatedObject,
                                     final StringBuilder exceptionBuilder) {
        final UpdateSalesCheckDto updatePart = validatedObject.getUpdatePart();
        if (updatePart.getName() != null && updatePart.getName().isEmpty()) {
            exceptionBuilder.append("\nUpdated name can't be empty.");
        }
        if (updatePart.getTypeId() != null && updatePart.getTypeId().isEmpty()) {
            exceptionBuilder.append("\nUpdated type id can't be empty.");
        }
        if (updatePart.getCompanyId() != null && updatePart.getCompanyId().isEmpty()) {
            exceptionBuilder.append("\nUpdated company id can't be empty.");
        }

        final Date currentDate = new Date();
        if (updatePart.getPurchaseDate() != null && currentDate.before(updatePart.getPurchaseDate())) {
            exceptionBuilder.append("\nUpdated date can't be more then current.");
        }

        final WarrantyDto warrantyDtoRequest = updatePart.getWarranty();
        if (warrantyDtoRequest != null) {
            final SalesCheckEntity salesCheck = salesCheckRepository.findByIdAndUserName(validatedObject.getId(), validatedObject.getUserName()).orElse(null);
            if (salesCheck == null) {
                exceptionBuilder
                        .append("\nHere is no a sales check with id ")
                        .append(validatedObject.getId())
                        .append(" and username ")
                        .append(validatedObject.getUserName() + ".");
            } else {
                final WarrantyEntity currentWarranty = warrantyRepository.findById(salesCheck.getWarrantyId()).orElse(new WarrantyEntity());
                final Date fromDate = warrantyDtoRequest.getFromDate() != null
                        ? warrantyDtoRequest.getFromDate()
                        : currentWarranty.getFromDate();
                final Date toDate = warrantyDtoRequest.getToDate() != null
                        ? warrantyDtoRequest.getToDate()
                        : currentWarranty.getToDate();
                if (fromDate != null && toDate != null && fromDate.after(toDate)) {
                    exceptionBuilder.append("\nIllegal \"from\" and \"to\" warranty dates.");
                }
            }
        }
    }
}
