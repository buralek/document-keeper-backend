package com.buralek.documentkeeper.salescheckservice.component.company.model;

import com.buralek.documentkeeper.salescheckservice.model.CompanyDto;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.StringJoiner;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GetCompanyResponse {
    private CompanyDto company;

    public CompanyDto getCompany() {
        return company;
    }

    public void setCompany(CompanyDto company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", GetCompanyResponse.class.getSimpleName() + "[", "]")
                .add("company=" + company)
                .toString();
    }
}
