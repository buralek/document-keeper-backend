package com.buralek.documentkeeper.salescheckservice.component.company.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.StringJoiner;

public class UpdateCompanyDto {
    private String name;
    private String address;
    private String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", UpdateCompanyDto.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("address='" + address + "'")
                .add("phone='" + phone + "'")
                .toString();
    }
}
