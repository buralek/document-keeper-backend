package com.buralek.documentkeeper.salescheckservice.model.mapper;

import com.buralek.documentkeeper.salescheckservice.model.SalesCheckDto;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.CompanyEntity;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.SalesCheckEntity;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.TypeEntity;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.WarrantyEntity;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.repository.CompanyRepository;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.repository.TypeRepository;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.repository.WarrantyRepository;
import org.springframework.stereotype.Service;

@Service
public class SalesCheckDtoMapperImpl implements SalesCheckDtoMapper {
    private final CompanyRepository companyRepository;
    private final TypeRepository typeRepository;
    private final WarrantyRepository warrantyRepository;
    private final TypeDtoMapper typeDtoMapper;
    private final CompanyDtoMapper companyDtoMapper;
    private final WarrantyDtoMapper warrantyDtoMapper;

    public SalesCheckDtoMapperImpl(final CompanyRepository companyRepository,
                                   final TypeRepository typeRepository,
                                   final WarrantyRepository warrantyRepository,
                                   final TypeDtoMapper typeDtoMapper,
                                   final CompanyDtoMapper companyDtoMapper,
                                   final WarrantyDtoMapper warrantyDtoMapper) {
        this.companyRepository = companyRepository;
        this.typeRepository = typeRepository;
        this.warrantyRepository = warrantyRepository;
        this.typeDtoMapper = typeDtoMapper;
        this.companyDtoMapper = companyDtoMapper;
        this.warrantyDtoMapper = warrantyDtoMapper;
    }
    @Override
    public SalesCheckDto map(final SalesCheckEntity salesCheckEntity) {
        final SalesCheckDto salesCheckDto = new SalesCheckDto();
        salesCheckDto.setId(salesCheckEntity.getId());
        salesCheckDto.setName(salesCheckEntity.getName());
        salesCheckDto.setUserName(salesCheckEntity.getUserName());
        salesCheckDto.setPurchaseDate(salesCheckEntity.getPurchaseDate());
        salesCheckDto.setCheckImages(salesCheckEntity.getCheckImages());
        salesCheckDto.setCompany(companyDtoMapper.map(companyRepository.findById(salesCheckEntity.getCompanyId()).orElse(new CompanyEntity())));
        salesCheckDto.setType(typeDtoMapper.map(typeRepository.findById(salesCheckEntity.getTypeId()).orElse(new TypeEntity())));
        salesCheckDto.setWarranty(warrantyDtoMapper.map(warrantyRepository.findById(salesCheckEntity.getWarrantyId()).orElse(new WarrantyEntity())));
        return salesCheckDto;
    }
}
