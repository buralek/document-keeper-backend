package com.buralek.documentkeeper.salescheckservice.component.salescheck.service;

import com.buralek.documentkeeper.salescheckservice.component.salescheck.model.AddSalesCheckRequest;
import com.buralek.documentkeeper.salescheckservice.component.salescheck.model.AddSalesCheckResponse;
import com.buralek.documentkeeper.salescheckservice.component.salescheck.model.AllSalesChecksResponse;
import com.buralek.documentkeeper.salescheckservice.component.salescheck.model.GetSalesChecksByIdsRequest;
import com.buralek.documentkeeper.salescheckservice.component.salescheck.model.GetSalesChecksByIdsResponse;
import com.buralek.documentkeeper.salescheckservice.component.salescheck.model.SalesCheckResponse;
import com.buralek.documentkeeper.salescheckservice.component.salescheck.model.UpdateSalesCheckRequest;
import com.buralek.documentkeeper.salescheckservice.component.salescheck.model.UpdateSalesCheckResponse;

public interface SalesCheckService {
    AddSalesCheckResponse addSalesCheck(AddSalesCheckRequest request);
    void deleteSalesCheck(String salesCheckId, String userName);
    GetSalesChecksByIdsResponse getSalesChecksByIds(GetSalesChecksByIdsRequest request);
    UpdateSalesCheckResponse updateSalesCheck(UpdateSalesCheckRequest request);
}
