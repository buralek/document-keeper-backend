package com.buralek.documentkeeper.salescheckservice.component.salescheck.model;

import java.util.List;
import java.util.StringJoiner;

public class GetSalesChecksByIdsRequest {
    private List<String> salesCheckIds;
    private String userName;

    public List<String> getSalesCheckIds() {
        return salesCheckIds;
    }

    public void setSalesCheckIds(List<String> salesCheckIds) {
        this.salesCheckIds = salesCheckIds;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", GetSalesChecksByIdsRequest.class.getSimpleName() + "[", "]")
                .add("salesCheckIds=" + salesCheckIds)
                .add("userName='" + userName + "'")
                .toString();
    }
}
