package com.buralek.documentkeeper.salescheckservice.model.mapper;

import com.buralek.documentkeeper.salescheckservice.model.TypeDto;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.TypeEntity;

public interface TypeDtoMapper {
    TypeDto map(final TypeEntity typeEntity);
}
