package com.buralek.documentkeeper.salescheckservice.component.salescheck.model;

import com.buralek.documentkeeper.salescheckservice.model.SalesCheckDto;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SalesCheckResponse {
    private SalesCheckDto salesCheck;

    public SalesCheckDto getSalesCheck() {
        return salesCheck;
    }

    public void setSalesCheck(SalesCheckDto salesCheck) {
        this.salesCheck = salesCheck;
    }

    @Override
    public String toString() {
        return "SalesCheckResponse{" +
                "salesCheck=" + salesCheck +
                '}';
    }
}
