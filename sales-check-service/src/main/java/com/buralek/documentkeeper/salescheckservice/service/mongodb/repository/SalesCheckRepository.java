package com.buralek.documentkeeper.salescheckservice.service.mongodb.repository;

import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.SalesCheckEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface SalesCheckRepository extends MongoRepository<SalesCheckEntity, String> {
    Optional<SalesCheckEntity> findByIdAndUserName(String id, String userName);
    void deleteByIdAndUserName(String id, String userName);
    List<SalesCheckEntity> findAllByUserName(String userName);

    @Query(value =  "{'_id':{$in: ?0 }, 'userName' : ?1})")
    List<SalesCheckEntity> findAllByIdAndUserName(Iterable<String> ids, String userName);
}
