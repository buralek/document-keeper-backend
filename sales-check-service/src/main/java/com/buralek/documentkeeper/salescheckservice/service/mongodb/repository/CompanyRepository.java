package com.buralek.documentkeeper.salescheckservice.service.mongodb.repository;

import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.CompanyEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CompanyRepository extends MongoRepository<CompanyEntity, String> {
    List<CompanyEntity> findAllByNameLike(String name);
}
