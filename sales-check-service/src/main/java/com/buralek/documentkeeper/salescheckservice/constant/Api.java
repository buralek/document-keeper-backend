package com.buralek.documentkeeper.salescheckservice.constant;

public class Api {
    public static final String SALES_CHECK_SERVICE_PATH = "/sales-check-service";
    public static final String SALES_CHECK_PATH = "/sales-check";
    public static final String ADD_PATH = "/add";
    public static final String DELETE_PATH = "/delete";
    public static final String ALL_PATH = "/all";
    public static final String SALES_CHECK_ID = "salesCheckId";
    public static final String USER_NAME = "userName";
    public static final String FILTER_PATH = "/filter";
    public static final String COMPANY_PATH = "/company";
    public static final String COMPANY_ID = "companyId";
    public static final String GET_BY_IDS_PATH = "/get-by-ids";
    public static final String UPDATE_PATH = "/update";
}
