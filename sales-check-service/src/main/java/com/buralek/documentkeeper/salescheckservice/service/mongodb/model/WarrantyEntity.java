package com.buralek.documentkeeper.salescheckservice.service.mongodb.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

@Document(collection = "warranty")
public class WarrantyEntity {
    @Id
    private String id;
    private Date fromDate;
    private Date toDate;
    private List<String> warrantyImages;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public List<String> getWarrantyImages() {
        return warrantyImages;
    }

    public void setWarrantyImages(List<String> warrantyImages) {
        this.warrantyImages = warrantyImages;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", WarrantyEntity.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("fromDate=" + fromDate)
                .add("toDate=" + toDate)
                .add("warrantyImages=" + warrantyImages)
                .toString();
    }
}
