package com.buralek.documentkeeper.salescheckservice.model.mapper;

import com.buralek.documentkeeper.salescheckservice.model.WarrantyDto;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.WarrantyEntity;
import org.springframework.stereotype.Service;

@Service
public class WarrantyDtoMapperImpl implements WarrantyDtoMapper {
    @Override
    public WarrantyDto map(final WarrantyEntity warrantyEntity) {
        final WarrantyDto warrantyDto = new WarrantyDto();
        warrantyDto.setId(warrantyEntity.getId());
        warrantyDto.setFromDate(warrantyEntity.getFromDate());
        warrantyDto.setToDate(warrantyEntity.getToDate());
        warrantyDto.setWarrantyImages(warrantyEntity.getWarrantyImages());
        return warrantyDto;
    }
}
