package com.buralek.documentkeeper.salescheckservice.component.salescheck.model;

import java.util.StringJoiner;

public class UpdateSalesCheckRequest {
    private String id;
    private String userName;
    private UpdateSalesCheckDto updatePart;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public UpdateSalesCheckDto getUpdatePart() {
        return updatePart;
    }

    public void setUpdatePart(UpdateSalesCheckDto updatePart) {
        this.updatePart = updatePart;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", UpdateSalesCheckRequest.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("userName='" + userName + "'")
                .add("updatePart=" + updatePart)
                .toString();
    }
}
