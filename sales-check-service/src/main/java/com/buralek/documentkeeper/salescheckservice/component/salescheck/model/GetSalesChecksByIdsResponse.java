package com.buralek.documentkeeper.salescheckservice.component.salescheck.model;

import com.buralek.documentkeeper.salescheckservice.model.SalesCheckDto;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GetSalesChecksByIdsResponse {
    private List<SalesCheckDto> salesChecks;

    public List<SalesCheckDto> getSalesChecks() {
        return salesChecks;
    }

    public void setSalesChecks(List<SalesCheckDto> salesChecks) {
        this.salesChecks = salesChecks;
    }

    @Override
    public String toString() {
        return "GetSalesChecksByIdsResponse{" +
                "salesChecks=" + salesChecks +
                '}';
    }
}
