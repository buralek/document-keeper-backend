package com.buralek.documentkeeper.salescheckservice.component.company.service;

import com.buralek.documentkeeper.salescheckservice.component.company.model.AddCompanyRequest;
import com.buralek.documentkeeper.salescheckservice.component.company.model.AddCompanyResponse;
import com.buralek.documentkeeper.salescheckservice.component.company.model.GetAllCompaniesResponse;
import com.buralek.documentkeeper.salescheckservice.component.company.model.GetCompanyResponse;
import com.buralek.documentkeeper.salescheckservice.component.company.model.UpdateCompanyRequest;
import com.buralek.documentkeeper.salescheckservice.component.company.model.UpdateCompanyResponse;

public interface CompanyService {
    GetCompanyResponse getCompany(String companyId);
    AddCompanyResponse addCompany(AddCompanyRequest request);
    void deleteCompany(String companyId);
    GetAllCompaniesResponse getAllCompanies();
    UpdateCompanyResponse updateCompany(UpdateCompanyRequest request);
}
