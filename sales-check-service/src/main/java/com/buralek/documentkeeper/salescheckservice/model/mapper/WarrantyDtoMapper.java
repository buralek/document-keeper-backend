package com.buralek.documentkeeper.salescheckservice.model.mapper;

import com.buralek.documentkeeper.salescheckservice.model.WarrantyDto;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.WarrantyEntity;

public interface WarrantyDtoMapper {
    WarrantyDto map(final WarrantyEntity warrantyEntity);
}
