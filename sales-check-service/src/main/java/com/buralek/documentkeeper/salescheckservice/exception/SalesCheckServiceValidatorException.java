package com.buralek.documentkeeper.salescheckservice.exception;

public class SalesCheckServiceValidatorException extends RuntimeException {
    public SalesCheckServiceValidatorException(String message) {
        super(message);
    }
}
