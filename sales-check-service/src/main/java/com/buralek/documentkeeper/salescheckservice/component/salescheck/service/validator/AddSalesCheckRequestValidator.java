package com.buralek.documentkeeper.salescheckservice.component.salescheck.service.validator;

import com.buralek.documentkeeper.salescheckservice.component.salescheck.model.AddSalesCheckRequest;
import com.buralek.documentkeeper.salescheckservice.exception.SalesCheckServiceValidatorException;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.repository.CompanyRepository;
import com.buralek.documentkeeper.salescheckservice.service.validator.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component("addSalesCheckRequestValidator")
public class AddSalesCheckRequestValidator implements Validator<AddSalesCheckRequest> {
    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public void validate(final AddSalesCheckRequest validatedObject) {
        // validate name
        final String name = validatedObject.getName();
        if (name == null || name.isEmpty()) {
            throw new SalesCheckServiceValidatorException("Wrong add sales check request. Name can't be empty.");
        }

        // validate company id
        final String companyId = validatedObject.getCompanyId();
        if (companyId == null || companyId.isEmpty()) {
            throw new SalesCheckServiceValidatorException("Wrong add sales check request. Company id can't be empty.");
        }
        companyRepository.findById(companyId)
                .orElseThrow(() -> new SalesCheckServiceValidatorException("Wrong add sales check request. Can't find company with id " + companyId + "."));

        // validate purchase date
        final Date purchaseDate = validatedObject.getPurchaseDate();
        if (purchaseDate == null) {
            throw new SalesCheckServiceValidatorException("Wrong add sales check request. Purchase date can't be empty.");
        }
        final Date currentDate = new Date();
        if (currentDate.before(purchaseDate)) {
            throw new SalesCheckServiceValidatorException("Wrong add sales check request. Purchase date can't be more then current.");
        }

        // validate userName
        final String userName = validatedObject.getUserName();
        if (userName == null || userName.isEmpty()) {
            throw new SalesCheckServiceValidatorException("Wrong add sales check request. User name can't be empty.");
        }

        // validate images
        // TODO
    }
}
