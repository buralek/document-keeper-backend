package com.buralek.documentkeeper.salescheckservice.service.mongodb.repository;

import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.TypeEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TypeRepository extends MongoRepository<TypeEntity, String> {
}
