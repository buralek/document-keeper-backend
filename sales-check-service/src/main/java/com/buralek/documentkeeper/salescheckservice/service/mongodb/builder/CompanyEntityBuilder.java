package com.buralek.documentkeeper.salescheckservice.service.mongodb.builder;

import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.CompanyEntity;

public interface CompanyEntityBuilder {
    CompanyEntityBuilder getBuilderInstance();

    CompanyEntity createObject();

    CompanyEntityBuilder setId(String id);

    CompanyEntityBuilder setName(String name);

    CompanyEntityBuilder setAddress(String address);

    CompanyEntityBuilder setPhone(String phone);

    CompanyEntity build();
}
