package com.buralek.documentkeeper.salescheckservice.component.company.model;

import com.buralek.documentkeeper.salescheckservice.model.CompanyDto;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;
import java.util.StringJoiner;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GetAllCompaniesResponse {
    private List<CompanyDto> companies;

    public List<CompanyDto> getCompanies() {
        return companies;
    }

    public void setCompanies(List<CompanyDto> companies) {
        this.companies = companies;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", GetAllCompaniesResponse.class.getSimpleName() + "[", "]")
                .add("companies=" + companies)
                .toString();
    }
}
