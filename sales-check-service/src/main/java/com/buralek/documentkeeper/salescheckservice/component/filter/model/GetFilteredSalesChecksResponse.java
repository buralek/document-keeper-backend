package com.buralek.documentkeeper.salescheckservice.component.filter.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;
import java.util.StringJoiner;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GetFilteredSalesChecksResponse {
    private List<String> filteredSalesCheckIds;

    public List<String> getFilteredSalesCheckIds() {
        return filteredSalesCheckIds;
    }

    public void setFilteredSalesCheckIds(List<String> filteredSalesCheckIds) {
        this.filteredSalesCheckIds = filteredSalesCheckIds;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", GetFilteredSalesChecksResponse.class.getSimpleName() + "[", "]")
                .add("filteredSalesCheckIds=" + filteredSalesCheckIds)
                .toString();
    }
}
