package com.buralek.documentkeeper.salescheckservice.component.salescheck.controller;

import com.buralek.documentkeeper.salescheckservice.component.salescheck.model.*;
import com.buralek.documentkeeper.salescheckservice.component.salescheck.service.SalesCheckService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static com.buralek.documentkeeper.salescheckservice.constant.Api.*;

@RestController
@RequestMapping(SALES_CHECK_SERVICE_PATH + SALES_CHECK_PATH)
@ApiOperation("Sales check controller")
public class SalesCheckController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SalesCheckController.class);

    private final SalesCheckService salesCheckService;

    public SalesCheckController(final SalesCheckService salesCheckService) {
        this.salesCheckService = salesCheckService;
    }

    @RequestMapping(method = RequestMethod.POST, value = ADD_PATH)
    @ApiOperation(httpMethod = "POST", value = "Add new sales check")
    @ResponseStatus(HttpStatus.CREATED)
    public AddSalesCheckResponse addSalesCheck(@RequestBody final AddSalesCheckRequest request) {
        LOGGER.debug("Add new sales check '{}'", request);
        final AddSalesCheckResponse response = salesCheckService.addSalesCheck(request);
        LOGGER.debug("New sales check was added successfully. Response is '{}'" + response);
        return response;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = DELETE_PATH)
    @ApiOperation(httpMethod = "DELETE", value = "Delete sales check by id")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteSalesCheck(@RequestParam(SALES_CHECK_ID) final String salesCheckId,
                                 @RequestParam(USER_NAME) final String userName) {
        LOGGER.debug("Delete sales check with id = '{}' for user with id '{}'", salesCheckId, userName);
        salesCheckService.deleteSalesCheck(salesCheckId, userName);
        LOGGER.debug("The sales check was deleted successfully");
    }

    @RequestMapping(method = RequestMethod.POST, path = GET_BY_IDS_PATH)
    @ApiOperation(httpMethod = "POST", value = "Get sales checks by ids")
    @ResponseStatus(HttpStatus.OK)
    public GetSalesChecksByIdsResponse getSalesChecksByIds(@RequestBody final GetSalesChecksByIdsRequest request) {
        LOGGER.debug("Get sales checks by ids '{}'", request.getSalesCheckIds());
        final GetSalesChecksByIdsResponse response = salesCheckService.getSalesChecksByIds(request);
        LOGGER.debug("Sales checks response by ids is '{}'", response);
        return response;
    }

    @RequestMapping(method = RequestMethod.PUT, path = UPDATE_PATH)
    @ApiOperation(httpMethod = "PUT", value = "Update sales check")
    @ResponseStatus(HttpStatus.OK)
    public UpdateSalesCheckResponse updateSalesCheck(@RequestBody final UpdateSalesCheckRequest request) {
        LOGGER.debug("Update sales checks regarding request '{}'", request);
        final UpdateSalesCheckResponse response = salesCheckService.updateSalesCheck(request);
        LOGGER.debug("Updated sales check is '{}'", response);
        return response;
    }
}
