package com.buralek.documentkeeper.salescheckservice.model.mapper;

import com.buralek.documentkeeper.salescheckservice.model.CompanyDto;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.CompanyEntity;

public interface CompanyDtoMapper {
    CompanyDto map(final CompanyEntity companyEntity);
}
