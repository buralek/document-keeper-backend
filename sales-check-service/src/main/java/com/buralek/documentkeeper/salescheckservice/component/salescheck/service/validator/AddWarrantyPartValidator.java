package com.buralek.documentkeeper.salescheckservice.component.salescheck.service.validator;

import com.buralek.documentkeeper.salescheckservice.exception.SalesCheckServiceValidatorException;
import com.buralek.documentkeeper.salescheckservice.model.WarrantyDto;
import com.buralek.documentkeeper.salescheckservice.service.validator.Validator;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component("warrantyPartValidator")
public class AddWarrantyPartValidator implements Validator<WarrantyDto> {

    @Override
    public void validate(final WarrantyDto validatedObject) {
        // validate fromDate and toDate
        final Date fromDate = validatedObject.getFromDate();
        final Date toDate = validatedObject.getToDate();
        if (fromDate == null) {
            throw new SalesCheckServiceValidatorException("Wrong add sales check request. Warranty \"from\" date can't be empty.");
        }
        if (toDate == null) {
            throw new SalesCheckServiceValidatorException("Wrong add sales check request. Warranty \"to\" date can't be empty.");
        }
        if (fromDate.after(toDate)) {
            throw new SalesCheckServiceValidatorException("Wrong add sales check request. Illegal \"from\" and \"to\" dates.");
        }

        // validate images
        //TODO
    }
}
