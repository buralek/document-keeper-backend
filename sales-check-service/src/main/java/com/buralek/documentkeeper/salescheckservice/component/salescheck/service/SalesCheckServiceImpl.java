package com.buralek.documentkeeper.salescheckservice.component.salescheck.service;

import com.buralek.documentkeeper.salescheckservice.component.salescheck.model.*;
import com.buralek.documentkeeper.salescheckservice.model.WarrantyDto;
import com.buralek.documentkeeper.salescheckservice.model.mapper.SalesCheckDtoMapper;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.builder.SalesCheckEntityBuilder;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.builder.SalesCheckEntityBuilderImpl;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.builder.WarrantyEntityBuilder;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.builder.WarrantyEntityBuilderImpl;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.SalesCheckEntity;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.WarrantyEntity;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.repository.SalesCheckRepository;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.repository.WarrantyRepository;
import com.buralek.documentkeeper.salescheckservice.service.validator.Validator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SalesCheckServiceImpl implements SalesCheckService {
    private final SalesCheckRepository salesCheckRepository;
    private final WarrantyRepository warrantyRepository;
    private final Validator addSalesCheckRequestValidator;
    private final Validator warrantyPartValidator;
    private final Validator updateSalesCheckRequestValidator;
    private final Validator getSalesChecksByIdsValidator;
    private final SalesCheckDtoMapper salesCheckDtoMapper;

    public SalesCheckServiceImpl(final SalesCheckRepository salesCheckRepository,
                                 final WarrantyRepository warrantyRepository,
                                 @Qualifier("addSalesCheckRequestValidator") final Validator addSalesCheckRequestValidator,
                                 @Qualifier("warrantyPartValidator") final Validator warrantyPartValidator,
                                 @Qualifier("updateSalesCheckRequestValidator") final Validator updateSalesCheckRequestValidator,
                                 @Qualifier("getSalesChecksByIdsValidator") final Validator getSalesChecksByIdsValidator,
                                 final SalesCheckDtoMapper salesCheckDtoMapper) {
        this.salesCheckRepository = salesCheckRepository;
        this.warrantyRepository = warrantyRepository;
        this.addSalesCheckRequestValidator = addSalesCheckRequestValidator;
        this.warrantyPartValidator = warrantyPartValidator;
        this.updateSalesCheckRequestValidator = updateSalesCheckRequestValidator;
        this.getSalesChecksByIdsValidator = getSalesChecksByIdsValidator;
        this.salesCheckDtoMapper = salesCheckDtoMapper;
    }

    @Override
    public AddSalesCheckResponse addSalesCheck(final AddSalesCheckRequest request) {
        addSalesCheckRequestValidator.validate(request);

        WarrantyEntity warrantyEntity = null;
        final WarrantyDto warrantyDtoRequest = request.getWarrantyDto();
        if (warrantyDtoRequest != null) {
            warrantyPartValidator.validate(warrantyDtoRequest);
            final WarrantyEntityBuilder warrantyEntityBuilder = new WarrantyEntityBuilderImpl();
            warrantyEntity = warrantyEntityBuilder
                    .setId(UUID.randomUUID().toString())
                    .setFromDate(warrantyDtoRequest.getFromDate())
                    .setToDate(warrantyDtoRequest.getToDate())
                    .build();
            warrantyRepository.save(warrantyEntity);
        }

        final SalesCheckEntityBuilder salesCheckEntityBuilder = new SalesCheckEntityBuilderImpl();
        final SalesCheckEntity salesCheckEntity = salesCheckEntityBuilder
                .setId(UUID.randomUUID().toString())
                .setName(request.getName())
                .setTypeId(request.getTypeId())
                .setPurchaseDate(request.getPurchaseDate())
                .setCompanyId(request.getCompanyId())
                .setUserName(request.getUserName())
                .setWarrantyId(warrantyEntity == null ? null : warrantyEntity.getId())
                .build();

        salesCheckRepository.save(salesCheckEntity);
        final AddSalesCheckResponse response = new AddSalesCheckResponse();
        response.setAddedSalesCheckId(salesCheckEntity.getId());
        return response;
    }

    @Override
    public void deleteSalesCheck(final String salesCheckId, final String userName) {
        salesCheckRepository.deleteByIdAndUserName(salesCheckId, userName);
    }

    @Override
    public GetSalesChecksByIdsResponse getSalesChecksByIds(final GetSalesChecksByIdsRequest request) {
        getSalesChecksByIdsValidator.validate(request);
        final GetSalesChecksByIdsResponse response = new GetSalesChecksByIdsResponse();
        if (!CollectionUtils.isEmpty(request.getSalesCheckIds())) {
            final List<SalesCheckEntity> salesChecks = salesCheckRepository.findAllByIdAndUserName(request.getSalesCheckIds(), request.getUserName());
            response.setSalesChecks(salesChecks.stream()
                    .map(salesCheckDtoMapper::map)
                    .collect(Collectors.toList()));
        }
        return response;
    }

    @Override
    public UpdateSalesCheckResponse updateSalesCheck(final UpdateSalesCheckRequest request) {
        updateSalesCheckRequestValidator.validate(request);
        final SalesCheckEntity salesCheck = salesCheckRepository.findByIdAndUserName(request.getId(), request.getUserName()).get(); // null value has already checked in the validator
        final UpdateSalesCheckDto updatePart = request.getUpdatePart();
        if (updatePart.getName() != null) {
            salesCheck.setName(updatePart.getName());
        }
        if (updatePart.getTypeId() != null) {
            salesCheck.setTypeId(updatePart.getTypeId());
        }
        if (updatePart.getCompanyId() != null) {
            salesCheck.setCompanyId(updatePart.getCompanyId());
        }
        if (updatePart.getPurchaseDate() != null) {
            salesCheck.setPurchaseDate(updatePart.getPurchaseDate());
        }

        final WarrantyDto warrantyDtoRequest = updatePart.getWarranty();
        if (warrantyDtoRequest != null) {
            final WarrantyEntity warranty = warrantyRepository.findById(salesCheck.getWarrantyId())
                    .orElseGet(() -> {
                        WarrantyEntityBuilder warrantyEntityBuilder = new WarrantyEntityBuilderImpl();
                        return warrantyEntityBuilder
                                .setId(UUID.randomUUID().toString())
                                .build();});
            if (warrantyDtoRequest.getFromDate() != null) {
                warranty.setFromDate(warrantyDtoRequest.getFromDate());
            }
            if (warrantyDtoRequest.getToDate() != null) {
                warranty.setToDate(warrantyDtoRequest.getToDate());
            }
            warrantyRepository.save(warranty);
        }

        salesCheckRepository.save(salesCheck);

        final UpdateSalesCheckResponse response = new UpdateSalesCheckResponse();
        response.setSalesCheck(salesCheckDtoMapper.map(salesCheck));
        return response;
    }
}
