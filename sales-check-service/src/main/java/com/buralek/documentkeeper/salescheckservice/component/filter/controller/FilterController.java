package com.buralek.documentkeeper.salescheckservice.component.filter.controller;


import com.buralek.documentkeeper.salescheckservice.component.filter.model.GetFilteredSalesChecksRequest;
import com.buralek.documentkeeper.salescheckservice.component.filter.model.GetFilteredSalesChecksResponse;
import com.buralek.documentkeeper.salescheckservice.component.filter.service.FilterService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static com.buralek.documentkeeper.salescheckservice.constant.Api.FILTER_PATH;
import static com.buralek.documentkeeper.salescheckservice.constant.Api.SALES_CHECK_SERVICE_PATH;

@RestController
@RequestMapping(SALES_CHECK_SERVICE_PATH + FILTER_PATH)
@ApiOperation("Filter controller")
public class FilterController {
    private static final Logger LOGGER = LoggerFactory.getLogger(FilterController.class);

    private final FilterService filterService;

    public FilterController(final FilterService filterService) {
        this.filterService = filterService;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(httpMethod = "POST", value = "Return filtered sales checks")
    @ResponseStatus(HttpStatus.OK)
    public GetFilteredSalesChecksResponse getFilteredSalesChecks(@RequestBody final GetFilteredSalesChecksRequest request) {
        LOGGER.debug("Return filtered sales checks by request '{}'", request);
        final GetFilteredSalesChecksResponse response = filterService.filterSalesChecks(request);
        LOGGER.debug("Filtered sales checks '{}'", response);
        return response;
    }
}
