package com.buralek.documentkeeper.salescheckservice.component.company.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.StringJoiner;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AddCompanyResponse {
    private String companyId;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", AddCompanyResponse.class.getSimpleName() + "[", "]")
                .add("companyId='" + companyId + "'")
                .toString();
    }
}
