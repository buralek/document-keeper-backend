package com.buralek.documentkeeper.salescheckservice.service.validator;

public interface Validator <T> {
    void validate(T validatedObject);
}
