package com.buralek.documentkeeper.salescheckservice.service.mongodb.builder;

import com.buralek.documentkeeper.salescheckservice.exception.SalesCheckServiceCreateDtoException;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.WarrantyEntity;

import java.util.Date;
import java.util.List;

public class WarrantyEntityBuilderImpl implements WarrantyEntityBuilder {
    private String id;
    private Date fromDate;
    private Date toDate;
    private List<String> warrantyImages;

    @Override
    public WarrantyEntityBuilder getBuilderInstance() {
        return this;
    }

    @Override
    public WarrantyEntity createObject() {
        return new WarrantyEntity();
    }

    @Override
    public WarrantyEntityBuilder setId(String id) {
        this.id = id;
        return getBuilderInstance();
    }

    @Override
    public WarrantyEntityBuilder setFromDate(Date fromDate) {
        this.fromDate = fromDate;
        return getBuilderInstance();
    }

    @Override
    public WarrantyEntityBuilder setToDate(Date toDate) {
        this.toDate = toDate;
        return getBuilderInstance();
    }

    @Override
    public WarrantyEntityBuilder setWarrantyImages(List<String> warrantyImages) {
        this.warrantyImages = warrantyImages;
        return getBuilderInstance();
    }

    @Override
    public WarrantyEntity build() {
        final WarrantyEntity warrantyEntity = createObject();
        if (id == null || id.isEmpty()) {
            throw new SalesCheckServiceCreateDtoException("Can't create WarrantyDto because Id is empty");
        }
        warrantyEntity.setId(id);
        warrantyEntity.setFromDate(fromDate);
        warrantyEntity.setToDate(toDate);
        warrantyEntity.setWarrantyImages(warrantyImages);
        return warrantyEntity;
    }
}
