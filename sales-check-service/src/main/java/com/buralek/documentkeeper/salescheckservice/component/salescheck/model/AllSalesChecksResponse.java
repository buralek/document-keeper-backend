package com.buralek.documentkeeper.salescheckservice.component.salescheck.model;

import com.buralek.documentkeeper.salescheckservice.model.SalesCheckDto;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;
import java.util.StringJoiner;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AllSalesChecksResponse {
    private List<SalesCheckDto> salesChecks;

    public List<SalesCheckDto> getSalesChecks() {
        return salesChecks;
    }

    public void setSalesChecks(List<SalesCheckDto> salesChecks) {
        this.salesChecks = salesChecks;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", AllSalesChecksResponse.class.getSimpleName() + "[", "]")
                .add("salesChecks=" + salesChecks)
                .toString();
    }
}
