package com.buralek.documentkeeper.salescheckservice.exception;

public class SalesCheckServiceCreateDtoException extends RuntimeException {
    public SalesCheckServiceCreateDtoException(String message) {
        super(message);
    }
}
