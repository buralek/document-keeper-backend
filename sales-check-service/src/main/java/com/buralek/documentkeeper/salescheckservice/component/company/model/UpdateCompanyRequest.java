package com.buralek.documentkeeper.salescheckservice.component.company.model;

import java.util.StringJoiner;

public class UpdateCompanyRequest {
    private String id;
    private UpdateCompanyDto updatePart;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UpdateCompanyDto getUpdatePart() {
        return updatePart;
    }

    public void setUpdatePart(UpdateCompanyDto updatePart) {
        this.updatePart = updatePart;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", UpdateCompanyRequest.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("updatePart=" + updatePart)
                .toString();
    }
}
