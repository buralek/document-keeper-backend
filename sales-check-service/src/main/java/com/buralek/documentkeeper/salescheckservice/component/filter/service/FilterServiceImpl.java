package com.buralek.documentkeeper.salescheckservice.component.filter.service;

import com.buralek.documentkeeper.salescheckservice.component.filter.model.GetFilteredSalesChecksRequest;
import com.buralek.documentkeeper.salescheckservice.component.filter.model.GetFilteredSalesChecksResponse;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.CompanyEntity;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.SalesCheckEntity;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.repository.CompanyRepository;
import com.buralek.documentkeeper.salescheckservice.service.validator.Validator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FilterServiceImpl implements FilterService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FilterServiceImpl.class);

    private final CompanyRepository companyRepository;

    private final Validator validator;

    private final MongoTemplate mongoTemplate;

    private final String SALES_CHECK_COLLECTION = "sales_check";
    private final String USERNAME = "userName";
    private final String ID = "_id";
    private final String NAME = "name";
    private final String TYPE_ID = "typeId";
    private final String COMPANY_ID = "companyId";
    private final String PURCHASE_DATE = "purchaseDate";

    public FilterServiceImpl(final CompanyRepository companyRepository,
                             final @Qualifier("FilterSalesChecksValidator") Validator validator,
                             final MongoTemplate mongoTemplate) {
        this.companyRepository = companyRepository;
        this.validator = validator;
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public GetFilteredSalesChecksResponse filterSalesChecks(final GetFilteredSalesChecksRequest request) {
        validator.validate(request);
        final GetFilteredSalesChecksResponse response = new GetFilteredSalesChecksResponse();
        final Query filteredQuery = createFilterQuery(request);
        LOGGER.debug("Filtered query = '{}'", filteredQuery);
        final List<SalesCheckEntity> filteredSalesChecks = mongoTemplate.find(filteredQuery, SalesCheckEntity.class, SALES_CHECK_COLLECTION);
        response.setFilteredSalesCheckIds(filteredSalesChecks.stream()
                .map(filteredSalesCheck -> filteredSalesCheck.getId())
                .collect(Collectors.toList()));

        return response;
    }

    private Query createFilterQuery(final GetFilteredSalesChecksRequest request) {
        final Query query = new Query();
        final List<Criteria> criterias = new ArrayList<>();
        Criteria filterCriteria = Criteria.where(USERNAME).is(request.getUserName());
        if (StringUtils.isNotEmpty(request.getId())) {
            criterias.add(Criteria.where(ID).is(request.getId()));
        }
        if (StringUtils.isNotEmpty(request.getName())) {
            criterias.add(Criteria.where(NAME).regex(".*" + request.getName() + ".*"));
        }
        if (StringUtils.isNotEmpty(request.getTypeId())) {
            criterias.add(Criteria.where(TYPE_ID).is(request.getTypeId()));
        }
        if (StringUtils.isNotEmpty(request.getCompanyName())) {
            final List<String> companyIds = companyRepository.findAllByNameLike(request.getCompanyName()).stream()
                    .map(CompanyEntity::getId)
                    .collect(Collectors.toList());
            criterias.add(Criteria.where(COMPANY_ID).in(companyIds));
        }
        final Criteria purchaseDateCriteria = createPurchaseDateCriteria(request);
        if (purchaseDateCriteria != null) {
            criterias.add(purchaseDateCriteria);
        }

        if (!CollectionUtils.isEmpty(criterias)) {
            filterCriteria = filterCriteria.andOperator(criterias.toArray(Criteria[]::new));
        }
        query.addCriteria(filterCriteria);
        return query;
    }

    private Criteria createPurchaseDateCriteria(final GetFilteredSalesChecksRequest request) {
        Criteria criteria = null;
        Criteria.where(PURCHASE_DATE).gte("").lte("");
        if (request.getFromDate() != null || request.getToDate() != null) {
           criteria = Criteria.where(PURCHASE_DATE);
            if (request.getFromDate() != null) {
                criteria = criteria.gte(request.getFromDate());
            }
            if (request.getToDate() != null) {
                criteria = criteria.lte(request.getToDate());
            }
        }
        return criteria;
    }
}
