package com.buralek.documentkeeper.salescheckservice.component.company.service;

import com.buralek.documentkeeper.salescheckservice.component.company.model.*;
import com.buralek.documentkeeper.salescheckservice.model.mapper.CompanyDtoMapper;
import com.buralek.documentkeeper.salescheckservice.model.CompanyDto;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.builder.CompanyEntityBuilder;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.builder.CompanyEntityBuilderImpl;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.CompanyEntity;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.repository.CompanyRepository;
import com.buralek.documentkeeper.salescheckservice.service.validator.Validator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository companyRepository;
    private final Validator updateCompanyRequestValidator;
    private final CompanyDtoMapper companyDtoMapper;

    CompanyServiceImpl(final CompanyRepository companyRepository,
                       @Qualifier("updateCompanyRequestValidator") final Validator updateCompanyRequestValidator,
                       final CompanyDtoMapper companyDtoMapper) {
        this.companyRepository = companyRepository;
        this.updateCompanyRequestValidator = updateCompanyRequestValidator;
        this.companyDtoMapper = companyDtoMapper;
    }

    @Override
    public GetCompanyResponse getCompany(final String companyId) {
        final GetCompanyResponse response = new GetCompanyResponse();
        final CompanyDto companyDto = companyDtoMapper.map(companyRepository.findById(companyId).orElse(new CompanyEntity()));
        response.setCompany(companyDto);
        return response;
    }

    @Override
    public AddCompanyResponse addCompany(final AddCompanyRequest request) {
        final CompanyEntityBuilder companyEntityBuilder = new CompanyEntityBuilderImpl();
        final CompanyEntity companyEntity = companyEntityBuilder
                .setId(UUID.randomUUID().toString())
                .setName(request.getName())
                .setAddress(request.getAddress())
                .setPhone(request.getPhone())
                .build();
        companyRepository.save(companyEntity);
        final AddCompanyResponse response = new AddCompanyResponse();
        response.setCompanyId(companyEntity.getId());
        return response;
    }

    @Override
    public void deleteCompany(final String companyId) {
        companyRepository.deleteById(companyId);
    }

    @Override
    public GetAllCompaniesResponse getAllCompanies() {
        final List<CompanyDto> companies = companyRepository.findAll().stream()
                .map(companyDtoMapper::map)
                .collect(Collectors.toList());
        final GetAllCompaniesResponse response = new GetAllCompaniesResponse();
        response.setCompanies(companies);
        return response;
    }

    @Override
    public UpdateCompanyResponse updateCompany(final UpdateCompanyRequest request) {
        updateCompanyRequestValidator.validate(request);
        final UpdateCompanyResponse response = new UpdateCompanyResponse();
        companyRepository.findById(request.getId()).ifPresent(companyEntity -> {
            final UpdateCompanyDto updatePart = request.getUpdatePart();
            if (updatePart.getName() != null) {
                companyEntity.setName(updatePart.getName());
            }
            if (updatePart.getAddress() != null) {
                companyEntity.setAddress(updatePart.getAddress());
            }
            if (updatePart.getPhone() != null) {
                companyEntity.setPhone(updatePart.getPhone());
            }
            companyRepository.save(companyEntity);
            response.setCompany(companyDtoMapper.map(companyEntity));
        });
        return response;
    }
}
