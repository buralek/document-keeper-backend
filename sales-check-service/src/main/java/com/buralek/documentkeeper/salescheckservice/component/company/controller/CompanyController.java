package com.buralek.documentkeeper.salescheckservice.component.company.controller;


import com.buralek.documentkeeper.salescheckservice.component.company.model.AddCompanyRequest;
import com.buralek.documentkeeper.salescheckservice.component.company.model.AddCompanyResponse;
import com.buralek.documentkeeper.salescheckservice.component.company.model.GetAllCompaniesResponse;
import com.buralek.documentkeeper.salescheckservice.component.company.model.GetCompanyResponse;
import com.buralek.documentkeeper.salescheckservice.component.company.model.UpdateCompanyRequest;
import com.buralek.documentkeeper.salescheckservice.component.company.model.UpdateCompanyResponse;
import com.buralek.documentkeeper.salescheckservice.component.company.service.CompanyService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static com.buralek.documentkeeper.salescheckservice.constant.Api.*;

@RestController
@RequestMapping(SALES_CHECK_SERVICE_PATH + COMPANY_PATH)
@ApiOperation("Company controller")
public class CompanyController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyController.class);

    private final CompanyService companyService;

    public CompanyController(final CompanyService companyService) {
        this.companyService = companyService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(httpMethod = "GET", value = "Get company by id")
    @ResponseStatus(HttpStatus.OK)
    public GetCompanyResponse getCompany(@RequestParam(COMPANY_ID) final String companyId) {
        LOGGER.debug("Get company with id = '{}'", companyId);
        final GetCompanyResponse response = companyService.getCompany(companyId);
        LOGGER.debug("Company response is '{}'", response);
        return response;
    }

    @RequestMapping(method = RequestMethod.POST, value = ADD_PATH)
    @ApiOperation(httpMethod = "POST", value = "Add new company")
    @ResponseStatus(HttpStatus.CREATED)
    public AddCompanyResponse addCompany(@RequestBody final AddCompanyRequest request) {
        LOGGER.debug("Add new company '{}'", request);
        final AddCompanyResponse response = companyService.addCompany(request);
        LOGGER.debug("New company was added successfully. Response is '{}'" + response);
        return response;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = DELETE_PATH)
    @ApiOperation(httpMethod = "DELETE", value = "Delete company by id")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@RequestParam(COMPANY_ID) final String companyId) {
        LOGGER.debug("Delete company with id = '{}'", companyId);
        companyService.deleteCompany(companyId);
        LOGGER.debug("The company was deleted successfully");
    }

    @RequestMapping(method = RequestMethod.GET, path = ALL_PATH)
    @ApiOperation(httpMethod = "GET", value = "Get all companies")
    @ResponseStatus(HttpStatus.OK)
    public GetAllCompaniesResponse getAllCompanies() {
        LOGGER.debug("Get all companies");
        final GetAllCompaniesResponse response = companyService.getAllCompanies();
        LOGGER.debug("All companies response is '{}'", response);
        return response;
    }

    @RequestMapping(method = RequestMethod.PUT, path = UPDATE_PATH)
    @ApiOperation(httpMethod = "PUT", value = "Update company")
    @ResponseStatus(HttpStatus.OK)
    public UpdateCompanyResponse updateCompany(@RequestBody final UpdateCompanyRequest request) {
        LOGGER.debug("Update company regarding request '{}'", request);
        final UpdateCompanyResponse response = companyService.updateCompany(request);
        LOGGER.debug("Updated company is '{}'", response);
        return response;
    }
}
