package com.buralek.documentkeeper.salescheckservice.service.mongodb.builder;

import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.WarrantyEntity;

import java.util.Date;
import java.util.List;

public interface WarrantyEntityBuilder {
    WarrantyEntityBuilder getBuilderInstance();

    WarrantyEntity createObject();

    WarrantyEntityBuilder setId(String id);

    WarrantyEntityBuilder setFromDate(Date fromDate);

    WarrantyEntityBuilder setToDate(Date toDate);

    WarrantyEntityBuilder setWarrantyImages(List<String> warrantyImages);

    WarrantyEntity build();
}
