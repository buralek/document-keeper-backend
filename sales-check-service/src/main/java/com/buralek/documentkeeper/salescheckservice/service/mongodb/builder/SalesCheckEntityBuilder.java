package com.buralek.documentkeeper.salescheckservice.service.mongodb.builder;

import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.SalesCheckEntity;

import java.util.Date;
import java.util.List;

public interface SalesCheckEntityBuilder {
    SalesCheckEntityBuilder getBuilderInstance();

    SalesCheckEntity createObject();

    SalesCheckEntityBuilder setId(String id);

    SalesCheckEntityBuilder setName(String name);

    SalesCheckEntityBuilder setTypeId(String typeId);

    SalesCheckEntityBuilder setCompanyId(String companyId);

    SalesCheckEntityBuilder setUserName(String userName);

    SalesCheckEntityBuilder setPurchaseDate(Date purchaseDate);

    SalesCheckEntityBuilder setCheckImages(List<String> checkImages);

    SalesCheckEntityBuilder setWarrantyId(String warrantyId);

    SalesCheckEntity build();
}
