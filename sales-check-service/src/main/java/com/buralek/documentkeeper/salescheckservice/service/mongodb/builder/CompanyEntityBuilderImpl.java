package com.buralek.documentkeeper.salescheckservice.service.mongodb.builder;

import com.buralek.documentkeeper.salescheckservice.exception.SalesCheckServiceCreateDtoException;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.CompanyEntity;

public class CompanyEntityBuilderImpl implements CompanyEntityBuilder {
    private String id;
    private String name;
    private String address;
    private String phone;

    @Override
    public CompanyEntityBuilder getBuilderInstance() {
        return this;
    }

    @Override
    public CompanyEntity createObject() {
        return new CompanyEntity();
    }

    @Override
    public CompanyEntityBuilder setId(String id) {
        this.id = id;
        return getBuilderInstance();
    }

    @Override
    public CompanyEntityBuilder setName(String name) {
        this.name = name;
        return getBuilderInstance();
    }

    @Override
    public CompanyEntityBuilder setAddress(String address) {
        this.address = address;
        return getBuilderInstance();
    }

    @Override
    public CompanyEntityBuilder setPhone(String phone) {
        this.phone = phone;
        return  getBuilderInstance();
    }

    @Override
    public CompanyEntity build() {
        final CompanyEntity companyEntity = createObject();
        if (id == null || id.isEmpty()) {
            throw new SalesCheckServiceCreateDtoException("Can't create CompanyDto because Id is empty");
        }
        companyEntity.setId(id);
        companyEntity.setName(name);
        companyEntity.setAddress(address);
        companyEntity.setPhone(phone);
        return companyEntity;
    }
}
