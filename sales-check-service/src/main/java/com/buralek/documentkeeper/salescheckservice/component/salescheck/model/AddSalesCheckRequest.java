package com.buralek.documentkeeper.salescheckservice.component.salescheck.model;

import com.buralek.documentkeeper.salescheckservice.model.WarrantyDto;

import java.util.Date;

public class AddSalesCheckRequest {
    private String name;
    private String typeId;
    private String companyId;
    private String userName;
    private Date purchaseDate;
    private WarrantyDto warrantyDto;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public WarrantyDto getWarrantyDto() {
        return warrantyDto;
    }

    public void setWarrantyDto(WarrantyDto warrantyDto) {
        this.warrantyDto = warrantyDto;
    }

    @Override
    public String toString() {
        return "AddSalesCheckRequest{" +
                "name='" + name + '\'' +
                ", typeId='" + typeId + '\'' +
                ", companyId='" + companyId + '\'' +
                ", userName='" + userName + '\'' +
                ", purchaseDate=" + purchaseDate +
                ", warrantyDto=" + warrantyDto +
                '}';
    }
}
