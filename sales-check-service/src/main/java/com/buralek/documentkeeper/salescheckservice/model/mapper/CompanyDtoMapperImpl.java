package com.buralek.documentkeeper.salescheckservice.model.mapper;

import com.buralek.documentkeeper.salescheckservice.model.CompanyDto;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.CompanyEntity;
import org.springframework.stereotype.Service;

@Service
public class CompanyDtoMapperImpl implements CompanyDtoMapper {
    @Override
    public CompanyDto map(final CompanyEntity companyEntity) {
        final CompanyDto companyDto = new CompanyDto();
        companyDto.setId(companyEntity.getId());
        companyDto.setName(companyEntity.getName());
        companyDto.setAddress(companyEntity.getAddress());
        companyDto.setPhone(companyEntity.getPhone());
        return companyDto;
    }
}
