package com.buralek.documentkeeper.salescheckservice.model.mapper;

import com.buralek.documentkeeper.salescheckservice.model.TypeDto;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.TypeEntity;
import org.springframework.stereotype.Service;

@Service
public class TypeDtoMapperImpl implements TypeDtoMapper {
    @Override
    public TypeDto map(final TypeEntity typeEntity) {
        final TypeDto typeDto = new TypeDto();
        typeDto.setId(typeEntity.getId());
        typeDto.setName(typeEntity.getName());
        return typeDto;
    }
}
