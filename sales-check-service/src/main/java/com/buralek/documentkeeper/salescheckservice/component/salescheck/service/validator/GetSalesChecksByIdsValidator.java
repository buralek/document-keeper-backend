package com.buralek.documentkeeper.salescheckservice.component.salescheck.service.validator;

import com.buralek.documentkeeper.salescheckservice.component.salescheck.model.GetSalesChecksByIdsRequest;
import com.buralek.documentkeeper.salescheckservice.exception.SalesCheckServiceValidatorException;
import com.buralek.documentkeeper.salescheckservice.service.validator.Validator;
import org.springframework.stereotype.Component;

@Component("getSalesChecksByIdsValidator")
public class GetSalesChecksByIdsValidator implements Validator<GetSalesChecksByIdsRequest> {
    @Override
    public void validate(final GetSalesChecksByIdsRequest validatedObject) {
        final String userName = validatedObject.getUserName();
        if (userName == null || userName.isEmpty()) {
            throw new SalesCheckServiceValidatorException("Wrong get sales check by ids request. UserName can't be empty.");
        }
    }
}
