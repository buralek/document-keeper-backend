package com.buralek.documentkeeper.salescheckservice.model.mapper;

import com.buralek.documentkeeper.salescheckservice.model.SalesCheckDto;
import com.buralek.documentkeeper.salescheckservice.service.mongodb.model.SalesCheckEntity;

public interface SalesCheckDtoMapper {
    SalesCheckDto map(final SalesCheckEntity salesCheckEntity);
}
