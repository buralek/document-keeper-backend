# Document Keeper Backend
The main idea of this application is to create a simple microservice architecture project which can help in management of different checks and warranties.
 
**Full start with the frontend part**

1) Go to document-keeper-frontend directory
2) Execute "docker build -t document-keeper-frontend ."
3) Go to document-keeper-backend directory
3) Build Document Keeper backend by maven
4) Execute "docker-compose up"
5) Go to http://localhost:4200/ and authorize with login "testUser" and password "testPassword"

**What you can do**

Of course, there are crazy number of problems now.
However, you can:
 - create new Sales Check(all fields are required)
 - create new Company
 - use filter(by default the filter show all Sales Checks)
 - change Sales Check and Company fields
 - delete Sales Check
 
**Big known issues**
- Need to add correct error handling on backend and frontend part. There is a correct handling of 401 error on frontend part only.
- Check required fields on "create-sales-check" page on frontend part
- Need to add possibility to download and upload Sales Check and Warranty images
- Need to add JUnit tests on backend part

