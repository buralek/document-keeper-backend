package com.buralek.documentkeeper.authservice.exception;

public class AuthServiceCreateDtoException extends RuntimeException {
    public AuthServiceCreateDtoException(String message) {
        super(message);
    }
}
