package com.buralek.documentkeeper.authservice.service.mongodb.builder;

import com.buralek.documentkeeper.authservice.exception.AuthServiceCreateDtoException;
import com.buralek.documentkeeper.authservice.service.mongodb.model.UserEntity;
import org.springframework.stereotype.Service;

@Service
public class UserEntityBuilderImpl implements UserEntityBuilder {
    private String username;
    private String password;

    @Override
    public UserEntityBuilder getBuilderInstance() {
        return this;
    }

    @Override
    public UserEntity createObject() {
        return new UserEntity();
    }

    @Override
    public UserEntityBuilder setUsername(String username) {
        this.username = username;
        return getBuilderInstance();
    }

    @Override
    public UserEntityBuilder setPassword(String password) {
        this.password = password;
        return getBuilderInstance();
    }

    @Override
    public UserEntity build() {
        UserEntity userEntity = createObject();
        if (username == null || username.isEmpty()) {
            throw new AuthServiceCreateDtoException("Username is empty");
        }
        if (password == null || password.isEmpty()) {
            throw new AuthServiceCreateDtoException("Password is empty");
        }
        userEntity.setUsername(username);
        userEntity.setPassword(password);
        return userEntity;
    }
}
