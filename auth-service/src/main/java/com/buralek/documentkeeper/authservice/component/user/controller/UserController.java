package com.buralek.documentkeeper.authservice.component.user.controller;

import com.buralek.documentkeeper.authservice.component.user.model.AddUserRequest;
import com.buralek.documentkeeper.authservice.component.user.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

import static com.buralek.documentkeeper.authservice.constant.Api.*;

@RestController
@RequestMapping(AUTH_SERVICE_PATH + USERS_PATH)
@ApiOperation("User controller")
public class UserController {

    private final UserService userService;

    public UserController(final UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.GET, value = CURRENT_PATH)
    @ApiOperation(httpMethod = "GET", value = "Get current user")
    public Principal getUser(final Principal principal) {
        return principal;
    }

    @RequestMapping(method = RequestMethod.POST, value = ADD_PATH)
    @ApiOperation(httpMethod = "POST", value = "Add new user")
    public void createUser(@RequestBody final AddUserRequest request) {
        userService.addUser(request);
    }

    @RequestMapping(method = RequestMethod.GET, value = LOGIN_PATH)
    @ApiOperation(httpMethod = "GET", value = "Login")
    public ResponseEntity login() {
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

