package com.buralek.documentkeeper.authservice.component.user.service;

import com.buralek.documentkeeper.authservice.component.user.model.AddUserRequest;
import com.buralek.documentkeeper.authservice.service.mongodb.builder.UserEntityBuilder;
import com.buralek.documentkeeper.authservice.service.mongodb.builder.UserEntityBuilderImpl;
import com.buralek.documentkeeper.authservice.service.mongodb.model.UserEntity;
import com.buralek.documentkeeper.authservice.service.mongodb.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void addUser(final AddUserRequest request) {
        final UserEntityBuilder userEntityBuilder = new UserEntityBuilderImpl();
        final UserEntity userEntity = userEntityBuilder
                .setPassword(request.getPassword())
                .setUsername(request.getUsername())
                .build();
        userRepository.save(userEntity);
    }
}
