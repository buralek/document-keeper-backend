package com.buralek.documentkeeper.authservice.service.mongodb.builder;

import com.buralek.documentkeeper.authservice.service.mongodb.model.UserEntity;

public interface UserEntityBuilder {
    UserEntityBuilder getBuilderInstance();

    UserEntity createObject();

    UserEntityBuilder setUsername(String username);

    UserEntityBuilder setPassword(String password);

    UserEntity build();
}
