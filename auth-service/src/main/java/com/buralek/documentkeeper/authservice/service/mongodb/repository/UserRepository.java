package com.buralek.documentkeeper.authservice.service.mongodb.repository;

import com.buralek.documentkeeper.authservice.service.mongodb.model.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<UserEntity, String> {
    Optional<UserEntity> findByUsername(String userName);
}
