package com.buralek.documentkeeper.authservice.component.user.service;

import com.buralek.documentkeeper.authservice.component.user.model.AddUserRequest;

public interface UserService {
    void addUser(AddUserRequest request);
}
