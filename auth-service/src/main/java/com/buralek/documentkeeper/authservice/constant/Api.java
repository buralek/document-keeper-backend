package com.buralek.documentkeeper.authservice.constant;

public class Api {
    public static final String AUTH_SERVICE_PATH = "/auth-service";
    public static final String USERS_PATH = "/users";
    public static final String CURRENT_PATH = "/current";
    public static final String ADD_PATH = "/add";
    public static final String LOGIN_PATH = "/login";
}
