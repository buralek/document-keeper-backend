db = db.getSiblingDB('authServiceDB');
db.createUser({
    user: "authUser",
    pwd: "authDBPassword",
    roles: [
        {
            role: "readWrite",
            db: "authServiceDB"
        }
    ]
});
db.createCollection("users");
db.users.insert({'username': 'testUser', 'password' : '$2y$12$M1V4wa06U1fbJRsbKpR/nOnF03mkXQTwptCrgRwqFSQK1rXPCIwxK '});
