package com.buralek.documentkeeper.accountservice.component.account.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GetAllAccountsResponse {
    private List<AccountDto> accounts;

    public List<AccountDto> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountDto> accounts) {
        this.accounts = accounts;
    }

    @Override
    public String toString() {
        return "GetAllAccountsResponse{" +
                "accounts=" + accounts +
                '}';
    }
}
