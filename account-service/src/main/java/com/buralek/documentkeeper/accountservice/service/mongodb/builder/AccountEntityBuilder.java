package com.buralek.documentkeeper.accountservice.service.mongodb.builder;

import com.buralek.documentkeeper.accountservice.service.mongodb.model.AccountEntity;

import java.util.Date;

public interface AccountEntityBuilder {
    AccountEntityBuilder getBuilderInstance();

    AccountEntity createObject();

    AccountEntityBuilder setId(String id);

    AccountEntityBuilder setFirstName(String firstName);

    AccountEntityBuilder setMiddleName(String middleName);

    AccountEntityBuilder setSecondName(String secondName);

    AccountEntityBuilder setCreatedDate(Date createdDate);

    AccountEntityBuilder setEmail(String email);

    AccountEntity build();
}
