package com.buralek.documentkeeper.accountservice.component.account.model.mapper;

import com.buralek.documentkeeper.accountservice.component.account.model.AccountDto;
import com.buralek.documentkeeper.accountservice.service.mongodb.model.AccountEntity;

public interface AccountDtoMapper {
    AccountDto map(AccountEntity accountEntity);
}
