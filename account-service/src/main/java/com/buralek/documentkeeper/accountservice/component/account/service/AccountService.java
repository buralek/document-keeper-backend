package com.buralek.documentkeeper.accountservice.component.account.service;

import com.buralek.documentkeeper.accountservice.component.account.model.AddAccountRequest;
import com.buralek.documentkeeper.accountservice.component.account.model.GetAccountResponse;
import com.buralek.documentkeeper.accountservice.component.account.model.GetAllAccountsResponse;

public interface AccountService {
    void addAccount(AddAccountRequest request);

    void deleteAccount(String accountId);

    GetAllAccountsResponse getAllAccounts();

    GetAccountResponse getAccount(String accountId);
}
