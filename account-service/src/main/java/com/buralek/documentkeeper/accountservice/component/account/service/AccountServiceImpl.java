package com.buralek.documentkeeper.accountservice.component.account.service;

import com.buralek.documentkeeper.accountservice.component.account.model.AccountDto;
import com.buralek.documentkeeper.accountservice.component.account.model.AddAccountRequest;
import com.buralek.documentkeeper.accountservice.component.account.model.GetAccountResponse;
import com.buralek.documentkeeper.accountservice.component.account.model.GetAllAccountsResponse;
import com.buralek.documentkeeper.accountservice.component.account.model.mapper.AccountDtoMapper;
import com.buralek.documentkeeper.accountservice.service.mongodb.builder.AccountEntityBuilder;
import com.buralek.documentkeeper.accountservice.service.mongodb.builder.AccountEntityBuilderImpl;
import com.buralek.documentkeeper.accountservice.service.mongodb.model.AccountEntity;
import com.buralek.documentkeeper.accountservice.service.mongodb.repository.AccountRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    private final AccountDtoMapper accountDtoMapper;

    public AccountServiceImpl(final AccountRepository accountRepository,
                              final AccountDtoMapper accountDtoMapper) {
        this.accountRepository = accountRepository;
        this.accountDtoMapper = accountDtoMapper;
    }

    @Override
    public void addAccount(final AddAccountRequest request) {
        final AccountEntityBuilder accountEntityBuilder = new AccountEntityBuilderImpl();
        final AccountEntity account = accountEntityBuilder
                .setId(UUID.randomUUID().toString())
                .setFirstName(request.getFirstName())
                .setMiddleName(request.getMiddleName())
                .setSecondName(request.getSecondName())
                .setEmail(request.getEmail())
                .setCreatedDate(request.getDate())
                .build();
        accountRepository.save(account);
    }

    @Override
    public void deleteAccount(final String accountId) {
        accountRepository.deleteById(accountId);
    }

    @Override
    public GetAllAccountsResponse getAllAccounts() {
        final GetAllAccountsResponse response = new GetAllAccountsResponse();
        final List<AccountDto> accounts =  accountRepository.findAll().stream()
                .map(accountDtoMapper::map)
                .collect(Collectors.toList());
        response.setAccounts(accounts);
        return response;
    }

    @Override
    public GetAccountResponse getAccount(final String accountId) {
        final GetAccountResponse response = new GetAccountResponse();
        final AccountDto accountDto= accountDtoMapper.map(accountRepository.findById(accountId).orElse(new AccountEntity()));
        response.setAccount(accountDto);
        return response;
    }
}
