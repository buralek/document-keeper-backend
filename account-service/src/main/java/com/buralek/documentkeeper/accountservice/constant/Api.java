package com.buralek.documentkeeper.accountservice.constant;

public class Api {
    public static final String ACCOUNT_SERVICE_PATH = "/account-service";
    public static final String ACCOUNT_PATH = "/account";
    public static final String ADD_PATH = "/add";
    public static final String DELETE_PATH = "/delete";
    public static final String ACCOUNT_ID = "/{accountId}";
    public static final String ALL_PATH = "/all";
}
