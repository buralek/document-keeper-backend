package com.buralek.documentkeeper.accountservice.component.account.controller;


import com.buralek.documentkeeper.accountservice.component.account.model.AddAccountRequest;
import com.buralek.documentkeeper.accountservice.component.account.model.GetAccountResponse;
import com.buralek.documentkeeper.accountservice.component.account.model.GetAllAccountsResponse;
import com.buralek.documentkeeper.accountservice.component.account.service.AccountService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static com.buralek.documentkeeper.accountservice.constant.Api.*;

@RestController
@RequestMapping(ACCOUNT_SERVICE_PATH + ACCOUNT_PATH)
@ApiOperation("Account controller")
public class AccountController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

    private final AccountService accountService;

    public AccountController(final AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping(method = RequestMethod.GET, path = ALL_PATH)
    @ApiOperation(httpMethod = "GET", value = "Get all accounts")
    public GetAllAccountsResponse getAllAccounts() {
        LOGGER.debug("Get all accounts");
        final GetAllAccountsResponse response = accountService.getAllAccounts();
        LOGGER.debug("All accounts response is '{}'", response);
        return response;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(httpMethod = "GET", value = "Get account by id")
    public GetAccountResponse getAccount(@PathVariable final String accountId) {
        LOGGER.debug("Get account with id = '{}'", accountId);
        final GetAccountResponse response = accountService.getAccount(accountId);
        LOGGER.debug("Account response is '{}'", response);
        return response;
    }

    @RequestMapping(method = RequestMethod.POST, value = ADD_PATH)
    @ApiOperation(httpMethod = "POST", value = "Add new account")
    @ResponseStatus(HttpStatus.CREATED)
    public void addAccount(@RequestBody final AddAccountRequest request) {
        LOGGER.debug("Add new account '{}'", request);
        accountService.addAccount(request);
        LOGGER.debug("New account added successfully");
    }

    @RequestMapping(method = RequestMethod.DELETE, value = DELETE_PATH + ACCOUNT_ID)
    @ApiOperation(httpMethod = "DELETE", value = "Delete account by id")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAccount(@PathVariable final String accountId) {
        LOGGER.debug("Delete account with id = '{}'", accountId);
        accountService.deleteAccount(accountId);
        LOGGER.debug("The account was deleted successfully");
    }
}
