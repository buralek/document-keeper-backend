package com.buralek.documentkeeper.accountservice.service.mongodb.builder;

import com.buralek.documentkeeper.accountservice.exception.AccountServiceCreateDtoException;
import com.buralek.documentkeeper.accountservice.service.mongodb.model.AccountEntity;

import java.util.Date;

public class AccountEntityBuilderImpl implements AccountEntityBuilder {
    private String id;
    private String firstName;
    private String middleName;
    private String secondName;
    private Date createdDate;
    private String email;

    @Override
    public AccountEntityBuilder getBuilderInstance() {
        return this;
    }

    @Override
    public AccountEntity createObject() {
        return new AccountEntity();
    }

    @Override
    public AccountEntityBuilder setId(String id) {
        this.id = id;
        return getBuilderInstance();
    }

    @Override
    public AccountEntityBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return getBuilderInstance();
    }

    @Override
    public AccountEntityBuilder setMiddleName(String middleName) {
        this.middleName = middleName;
        return getBuilderInstance();
    }

    @Override
    public AccountEntityBuilder setSecondName(String secondName) {
        this.secondName = secondName;
        return getBuilderInstance();
    }

    @Override
    public AccountEntityBuilder setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
        return getBuilderInstance();
    }

    @Override
    public AccountEntityBuilder setEmail(String email) {
        this.email = email;
        return getBuilderInstance();
    }

    @Override
    public AccountEntity build() {
        final AccountEntity accountEntity = createObject();
        if (id == null || id.isEmpty()) {
            throw new AccountServiceCreateDtoException("Id is empty");
        }
        accountEntity.setId(id);
        accountEntity.setFirstName(firstName);
        accountEntity.setMiddleName(middleName);
        accountEntity.setSecondName(secondName);
        accountEntity.setCreatedDate(createdDate);
        accountEntity.setEmail(email);
        return accountEntity;
    }

}
