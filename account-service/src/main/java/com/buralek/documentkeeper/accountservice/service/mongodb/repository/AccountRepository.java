package com.buralek.documentkeeper.accountservice.service.mongodb.repository;

import com.buralek.documentkeeper.accountservice.service.mongodb.model.AccountEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AccountRepository extends MongoRepository<AccountEntity, String> {
}
