package com.buralek.documentkeeper.accountservice.component.account.model.mapper;

import com.buralek.documentkeeper.accountservice.component.account.model.AccountDto;
import com.buralek.documentkeeper.accountservice.service.mongodb.model.AccountEntity;
import org.springframework.stereotype.Service;

@Service
public class AccountDtoMapperImpl implements AccountDtoMapper {

    @Override
    public AccountDto map(final AccountEntity accountEntity) {
        final AccountDto accountDto = new AccountDto();
        accountDto.setId(accountEntity.getId());
        accountDto.setFirstName(accountEntity.getFirstName());
        accountDto.setMiddleName(accountEntity.getMiddleName());
        accountDto.setSecondName(accountEntity.getSecondName());
        accountDto.setEmail(accountEntity.getEmail());
        accountDto.setCreatedDate(accountEntity.getCreatedDate());
        return accountDto;
    }
}
