package com.buralek.documentkeeper.accountservice.component.account.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GetAccountResponse {
    private AccountDto account;

    public AccountDto getAccount() {
        return account;
    }

    public void setAccount(AccountDto account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "GetAccountResponse{" +
                "account=" + account +
                '}';
    }
}
