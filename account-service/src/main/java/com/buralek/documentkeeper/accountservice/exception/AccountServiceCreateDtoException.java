package com.buralek.documentkeeper.accountservice.exception;

public class AccountServiceCreateDtoException extends RuntimeException {
    public AccountServiceCreateDtoException(String message) {
        super(message);
    }
}
