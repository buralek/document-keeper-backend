db.createUser({
    user: "accountUser",
    pwd: "accountDBPassword",
    roles: [
        {
            role: "readWrite",
            db: "accountServiceDB"
        }
    ]
})
db = db.getSiblingDB('accountServiceDB')
db.createCollection("account")
